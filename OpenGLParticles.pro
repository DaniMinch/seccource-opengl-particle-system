QT       += opengl

CONFIG	 += C++14

QMAKE_LFLAGS_RELEASE += -static -static-libgcc

TARGET = OpenGLParticles
TEMPLATE = app


SOURCES += main.cpp \
    vec3f.cpp \
    particle.cpp \
    imageloader.cpp \
    particlesystem.cpp \
    materialcreator.cpp

HEADERS += \
    vec3f.h \
    particle.h \
    imageloader.h \
    particlesystem.h \
    timer.h \
    materialcreator.h

LIBS += libfreeglut -lopengl32 -lglu32
