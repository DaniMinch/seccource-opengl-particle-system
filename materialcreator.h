#ifndef MATERIALCREATOR
#define MATERIALCREATOR

#include <vector>
#include "vec3f.h" // вектор 3 флоатов

namespace MaterialCreator {
std::vector<std::vector<float>> createPlasticMaterial(Vec3f _color, float _alpha);

std::vector<std::vector<float>> createRubberMaterial(Vec3f _color, float _alpha);

std::vector<std::vector<float>> createGoldMaterial(float _alpha);

std::vector<std::vector<float>> createSilverMaterial(float _alpha);

std::vector<std::vector<float>> createEmeraldMaterial(float _alpha);

}
#endif // MATERIALCREATOR

