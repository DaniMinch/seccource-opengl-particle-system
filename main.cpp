#include <GL/freeglut.h>

#include "particlesystem.h"
#include "imageloader.h"


GLfloat xangle1 = 0;
GLfloat yangle1 = 0;
GLfloat zangle1 = 0;

GLuint _textureId; // текстура и квадрик для лаб 1 и 2
ParticleSystem* part_sys;

void display()
{
    // clear window
    glClearColor(0.2, 0.2, 0.2, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



    // draw scene
    glPushMatrix();

    glRotatef (xangle1, 1.f, 0.f, 0.f);  // Up and down arrow keys 'tip' view.
    glRotatef (yangle1, 0.f, 1.f, 0.f);  // Right/left arrow keys 'turn' view.
    glRotatef (zangle1, 0.f, 0.f, 1.f );  // pgup/pgdwn keys

    // начало координат
    glMaterialfv(GL_FRONT, GL_DIFFUSE, new float[4]{1.0f, 1.0f, 1.0f, 1.0f});
    glutSolidCube(0.01);

    if(part_sys->isRunning()&&part_sys->getUpdateFlag())
        part_sys->updateParticles();
    part_sys->initScene();

    glPopMatrix();

    // flush drawing routines to the window
    glFlush();
}

void Special_Keys (int key, int x, int y)
{

    switch (key) {

    case GLUT_KEY_LEFT :  yangle1 -= 2.0f;  break;
    case GLUT_KEY_RIGHT:  yangle1 += 2.0f;  break;
    case GLUT_KEY_UP   :  xangle1 -= 2.0f;  break;
    case GLUT_KEY_DOWN :  xangle1 += 2.0f;  break;
    case GLUT_KEY_PAGE_DOWN: zangle1 -= 2.0f; break;
    case GLUT_KEY_PAGE_UP: zangle1 += 2.0f; break;
    case GLUT_KEY_F1: part_sys->start(); break;
    case GLUT_KEY_F2: part_sys->stop(); break;
    default: return;
    }
    glutPostRedisplay();
}

// обновляем выделенную область. остальное нафиг
void reshape ( int width, int height ) {

    /* define the viewport transformation */
    glViewport(0,0,width,height);
}

GLuint loadTexture(Image* image) {
    GLuint textureId; // текстурки характеризуются id
    glGenTextures(1, &textureId); // включаем генерацию текстурных координат
    glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
    //Map the image to the texture
    glTexImage2D(GL_TEXTURE_2D,           //Always GL_TEXTURE_2D
                 0,                            //0 for now
                 GL_RGB,                       //Format OpenGL uses for image
                 image->width, image->height,  //Width and height
                 0,                            //The border of the image
                 GL_RGB, //GL_RGB, because pixels are stored in RGB format
                 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
                 //as unsigned numbers
                 image->pixels);               //The actual pixel data

    return textureId; //Returns the id of the texture
}

float light0_position[] = {0.0f, 0.f, .5f, 1.0f};
float light1_position[] = {2.0f, 1.f, -1.0f, 1.0f};
float light2_position[] = {-2.0f, 1.0f, 0.0f, 0.0f};
float light3_position[] = {2.0f, -2.0f, 2.0f, 1.0f};
float light4_position[] = {-2.0f, 2.0f, -2.0f, 1.0f};

// здесь прописываются параметры света
void createLights()
{

    glEnable(GL_LIGHTING); // включаем освещение
    // точечный
    float light_diffuse[] = {1.f, 1.f, 1.f, 1.f}; // цвет
    float light_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f}; // ???
    float light_specular[] = {1.0f, 1.0f, 1.0f, 1.0f}; // цвет отражаемого

    glEnable(GL_LIGHT0); // разрешаем использовать light0

    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

    glEnable(GL_LIGHT1); // разрешаем использовать light1

    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

    glEnable(GL_LIGHT2); // разрешаем использовать light2

    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT2, GL_POSITION, light2_position);

    glEnable(GL_LIGHT3); // разрешаем использовать light3

    glLightfv(GL_LIGHT3, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT3, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT3, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT3, GL_POSITION, light3_position);

    glEnable(GL_LIGHT4); // разрешаем использовать light4

    glLightfv(GL_LIGHT4, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT4, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT4, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT4, GL_POSITION, light4_position);

    glShadeModel(GL_SMOOTH);

}


int main ( int argc, char * argv[] ) {

    //initialize GLUT, using any commandline parameters passed to the program
    glutInit(&argc,argv);

    // setup the size, position, and display mode for new windows
    glutInitWindowSize(800, 800);
    glutInitWindowPosition(0,0);
    // говорим, что надо создать буфферы цвета и глубины
    glutInitDisplayMode(GLUT_RGB| GLUT_DEPTH);


    // create and set up a window
    glutCreateWindow("Particle System");
    {
        // инициализируем квадрик, говорит генерить текст. координаты и быть проволочным


        /*// загружаем текстуру imageloader'ом
        Image* image = loadBMP("pict2.bmp");
        _textureId = loadTexture(image);
        // удаляем бмп с текстуркой
        delete image;*/

        // применяем функции к окошку
        glutReshapeFunc(reshape);
        glutDisplayFunc(display);
        glutSpecialFunc(Special_Keys);


        // куллфейс позволяет скрывать невидимые грани. по умолчанию скрывает видимые грани
        //    glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST); // включаем глубину изображения
        glEnable(GL_ALPHA_TEST); // включаем чувствительность к альфа каналу(прозрачность)
        glEnable(GL_BLEND); // смешение цветов
        glEnable(GL_NORMALIZE); // нормализация нормалей
        glShadeModel(GL_SMOOTH); // мягкие тени. теней нет, so...
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // алгоритм смешения цвевтов
        glDisable(GL_COLOR_MATERIAL); // выключаем окраску color'ом -> будем красить материалами

        glMatrixMode(GL_PROJECTION); // устанавливаем матрицу проекции
        glLoadIdentity();
        gluPerspective(40,1,1,20); // угол обзора камеры, ???, передняя и задняя границы вида

        glMatrixMode(GL_MODELVIEW); // все следует делать в этой матрице
        glLoadIdentity();

        // позиция камеры
        gluLookAt(0.0,0.0,3.0, // позиция камеры
                  0.0,0.0,0.0, // центр точки обзора
                  0.0,1.0,0.0); // что-то с осями, лучше не трогать
    }

    /*{
        part_sys = new ParticleSystem(500, 1000, 50, 4);

      enum PS_OBJECT_TYPE
{
    PS_POINT, // x_start, y_start, x_finish, y_finish, z_finish
    PS_PLANE, // length, width, unused,slices, stacks
    PS_SPHERE, // radius, unused, unused, slices, stacks
    PS_CUBE, //radius, unused,unused, slices, stacks
    PS_CONE, //base radius, top radius = 0, height, slices, stacks
    PS_CYLINDER //base radius, top radius, height, slices, stacks
};
*/


    // Вариант 1
    /*{
        part_sys = new ParticleSystem(1000, 1000, 50, 7, 100, 1);
        float settings[5]= {-1.0f, 0.0f, 1.0f, 1.0f, 1.0f};
        part_sys->generateObject(new PSObject(PS_POINT,PS_GENERATOR,
                                              Vec3f(0.0f, 0.0f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings),
                                 true, PART_EMERALD, Vec3f(1.f, 0.f, 0.f), 1.f);
        float settings3 [5] = {0.6f, 0.6f, 0.8f, 2.f, 2.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings3),
                                 true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_SPEED, PS_DISTANCE, PS_NEGATIVE);
    }*/

    // Вариант 4
    /*{
        part_sys = new ParticleSystem(500, 1000, 20, 7, 200, 2);
        float settings[5]= {-1.0f, 0.0f, 1.0f, 1.0f, 1.0f};
        part_sys->generateObject(new PSObject(PS_POINT,PS_ANTIATTRACTOR,
                                              Vec3f(0.0f, 0.0f, 0.5f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings),
                                 false, PART_GOLD, Vec3f(1.f, 0.f, 0.f), 1.f);
        float settings1 [5] = {0.2f, 0.6f, 0.4f, 32.f, 32.0f};
        part_sys->generateObject(new PSObject(PS_CONE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings1),
                                 false, PART_RUBBER, Vec3f(0.0f, 0.81f, .82f), 1.f);
        float settings3 [5] = {0.6f, 0.6f, 0.8f, 2.f, 2.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.0f, 0.25f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings3),
                                 true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_SIZE, PS_DISTANCE, PS_NEGATIVE);
        part_sys->enable(PS_BRIGTHNESS, PS_DISTANCE, PS_NEGATIVE);
    }*/

    // Вариант 5
    /*{
        part_sys = new ParticleSystem(500, 1000, 50, 5, 100, 2);
        float settings[5]= {0.2f, 0.2f, 0.4f, 32.0f, 32.0f};
        part_sys->generateObject(new PSObject(PS_CYLINDER,PS_SURFACE,
                                              Vec3f(0.0f, 0.0f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 00.f,
                                              settings),
                                 true, PART_PLASTIC, Vec3f(1.f, 0.f, 0.f), 1.f);
        float settings2[5]= {0.5f, 0.5f, 0.4f, 32.0f, 32.0f};
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.25f, -0.5f),
                                              Vec3f(1.0f, 0.0f, 0.0f), 90.f,
                                              settings2),
                                 false, PART_PLASTIC, Vec3f(1.f, 1.f, 0.f), 1.f);
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.25f, 0.0f, -0.5f),
                                              Vec3f(0.0f, 1.0f, 0.0f), -90.f,
                                              settings2),
                                 false, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_SIZE, PS_DISTANCE, PS_NEGATIVE);
        part_sys->enable(PS_SPEED, PS_DISTANCE, PS_NEGATIVE);
    }*/

    // Вариант 6
    /*{
        part_sys = new ParticleSystem(500, 1000, 50, 4);
        float settings [5] = {0.5f, 0.5f, 0.8f, 32.f, 32.0f};
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, -0.6f),
                                              Vec3f(1.0f, 0.0f, 0.0f), 90.f,
                                              settings),
                                 false, PART_PLASTIC, Vec3f(1.f, 0.f, 0.f), 1.f);
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, -.6f),
                                              Vec3f(0.0f, 0.0f, 1.0f), 90.f,
                                              settings),
                                 false, PART_GOLD, Vec3f(1.f, 0.f, 0.f), 1.f);
        float settings2 [5] = {0.7f, 0.7f, 0.8f, 1.f, 1.0f};
        part_sys->generateObject(new PSObject(PS_PLANE,PS_SURFACE,
                                              Vec3f(0.0f, 0.f, 0.0f),
                                              Vec3f(1.0f, 0.0f, 0.0f), 45.f,
                                              settings2),
                                 false, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_SIZE, PS_DISTANCE, PS_POSITIVE);
        part_sys->enable(PS_SPEED, PS_DISTANCE, PS_POSITIVE);
    }*/

    // Вариант 8
    /*{
        part_sys = new ParticleSystem(1000, 100, 50, 0, 100, 1);
        float settings[5]= {-1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
        part_sys->generateObject(new PSObject(PS_POINT,PS_GENERATOR,
                                              Vec3f(0.3f, -0.4f, 0.1f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings),
                                 false, PART_RUBBER, Vec3f(1.f, 0.54f, 0.f), 1.f);
        part_sys->generateObject(new PSObject(PS_POINT,PS_GENERATOR,
                                              Vec3f(-0.3f, -0.4f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings),
                                 false, PART_PLASTIC, Vec3f(0.f, 1.f, 1.f), 1.f);
        part_sys->generateObject(new PSObject(PS_POINT,PS_ANTIATTRACTOR,
                                              Vec3f(0.5f, 0.0f, 0.5f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings),
                                 false, PART_PLASTIC, Vec3f(1.f, 0.f, 0.f), 1.f);
        float settings3 [5] = {0.6f, 0.6f, 0.8f, 2.f, 2.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.0f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings3),
                                 true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_SPEED, PS_LIFETIME, PS_POSITIVE);
        part_sys->enable(PS_ALPHA, PS_SPEED_COND, PS_POSITIVE);

    }*/

    // Вариант 18
    /*{
        part_sys = new ParticleSystem(500, 400, 50, 4);
        float settings [5] = {0.5f, 0.5f, 0.8f, 32.f, 32.0f};
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, -0.5f),
                                              Vec3f(1.0f, 0.0f, 0.0f), 90.f,
                                              settings),
                                 false, PART_PLASTIC, Vec3f(1.f, 0.f, 0.0f), 1.f);
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, -.5f),
                                              Vec3f(0.0f, 0.0f, 1.0f), 90.f,
                                              settings),
                                 false, PART_GOLD, Vec3f(1.f, 0.f, 0.f), 1.f);
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.0f, -.5f),
                                              Vec3f(1.0f, 0.0f, 0.0f), 180.f,
                                              settings),
                                 false, PART_PLASTIC, Vec3f(0.5f, 1.0f, 0.0f), 1.f);
        part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, -.5f),
                                              Vec3f(1.0f, 0.0f, 0.0f), -90.f,
                                              settings),
                                 false, PART_RUBBER, Vec3f(1.f, 0.f, 1.f), 1.f);
        float settings2[5] = {0.6f, 0.6f, 0.8f, 4.f, 4.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings2),
                                 true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        float settings3[5] = {0.2f, 0.6f, 0.8f, 2.f, 2.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.0f, 0.0f),
                                              Vec3f(1.0f, 1.0f, 0.0f), 60.f,
                                              settings3),
                                 false, PART_PLASTIC, Vec3f(0.f, 0.f, 1.f), 1.f);
        part_sys->enable(PS_REDCOLOR, PS_LIFETIME, PS_NEGATIVE);
        part_sys->enable(PS_BLUECOLOR, PS_LIFETIME, PS_POSITIVE);
        part_sys->enable(PS_ALPHA, PS_LIFETIME, PS_NEGATIVE);
    }*/

    // Вариант 21
    /*{
        part_sys = new ParticleSystem(1000, 300, 50, 7, 200, 3);
        float settings_sphere [5] = {0.2f, 0.7f, 0.8f, 32.f, 32.0f};
        part_sys->generateObject(new PSObject(PS_SPHERE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.0f, 0.0f),
                                              Vec3f(1.0f, 1.0f, 0.0f), 45.f,
                                              settings_sphere),
                                 true, PART_EMERALD, Vec3f(1.f, 0.f, 0.f), 1.f);
        float settings3 [5] = {0.6f, 0.6f, 0.8f, 2.f, 2.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings3),
                                 true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_ALPHA, PS_LIFETIME, PS_NEGATIVE);
        part_sys->enable(PS_SIZE, PS_LIFETIME, PS_NEGATIVE);
        part_sys->enable(PS_GREENCOLOR, PS_LIFETIME, PS_NEGATIVE);
        part_sys->enable(PS_BLUECOLOR, PS_LIFETIME, PS_POSITIVE);
        part_sys->enable(PS_REDCOLOR, PS_LIFETIME, PS_POSITIVE);
    }*/

    // Вариант 28
    /*{
        part_sys = new ParticleSystem(500, 1000, 20, 7, 200, 1);
        float settings1 [5] = {0.2f, 0.6f, 0.4f, 32.f, 32.0f};
        part_sys->generateObject(new PSObject(PS_CONE,PS_GENERATOR,
                                              Vec3f(0.0f, 0.f, 0.0f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings1),
                                 false, PART_RUBBER, Vec3f(0.5f, 1.0f, 0.5f), 1.f);
        float settings3 [5] = {0.6f, 0.6f, 0.8f, 2.f, 2.0f};
        part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                              Vec3f(0.0f, 0.0f, 0.25f),
                                              Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                              settings3),
                                 true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        part_sys->enable(PS_SPEED, PS_DISTANCE, PS_POSITIVE);
        part_sys->enable(PS_GREENCOLOR, PS_DISTANCE, PS_NEGATIVE);
        part_sys->enable(PS_REDCOLOR, PS_LIFETIME, PS_NEGATIVE);
    }*/

    // TEST
    {
    // 4 плоскости
        {
            part_sys = new ParticleSystem(500, 1000, 50, 4, 200, 5);
            float settings [5] = {0.7f, 0.7f, 0.8f, 32.f, 32.0f};
            part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                                  Vec3f(0.0f, 0.f, -0.5f),
                                                  Vec3f(1.0f, 0.0f, 0.0f), 90.f,
                                                  settings),
                                     false, PART_PLASTIC, Vec3f(1.f, 0.f, 0.f), 1.f);
            part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                                  Vec3f(0.0f, 0.f, -.5f),
                                                  Vec3f(0.0f, 0.0f, 1.0f), 90.f,
                                                  settings),
                                     false, PART_GOLD, Vec3f(1.f, 0.f, 0.f), 1.f);
            part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                                  Vec3f(0.0f, 0.0f, -.5f),
                                                  Vec3f(1.0f, 0.0f, 0.0f), 180.f,
                                                  settings),
                                     false, PART_SILVER, Vec3f(1.f, 0.f, 0.f), 1.f);
            part_sys->generateObject(new PSObject(PS_PLANE,PS_GENERATOR,
                                                  Vec3f(0.0f, 0.f, -.5f),
                                                  Vec3f(1.0f, 0.0f, 0.0f), -90.f,
                                                  settings),
                                     false, PART_RUBBER, Vec3f(0.f, 1.f, 1.f), 1.f);
        }

        // сфера в начале координат
        {
            float settings_sphere [5] = {0.2f, 0.7f, 0.8f, 32.f, 32.0f};
            part_sys->generateObject(new PSObject(PS_SPHERE,PS_ANTIATTRACTOR,
                                                  Vec3f(0.0f, 0.0f, 0.0f),
                                                  Vec3f(1.0f, 1.0f, 0.0f), 45.f,
                                                  settings_sphere),
                                     true, PART_EMERALD, Vec3f(1.f, 0.f, 0.f), 1.f);
        }

        // квадрат обрамляющий прямые и сферу
        {
            float settings3 [5] = {0.6f, 0.6f, 0.8f, 2.f, 2.0f};
            part_sys->generateObject(new PSObject(PS_CUBE,PS_SURFACE,
                                                  Vec3f(0.0f, 0.f, 0.0f),
                                                  Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                                  settings3),
                                     true, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        }

        // плоскость для отражения плоскостей
        {
//            float settings2 [5] = {0.7f, 0.7f, 0.8f, 32.f, 32.0f};
//            part_sys->generateObject(new PSObject(PS_PLANE,PS_SURFACE,
//                                                  Vec3f(0.0f, 0.f, 0.0f),
//                                                  Vec3f(1.0f, 0.0f, 0.0f), 45.f,
//                                                  settings2),
//                                     false, PART_PLASTIC, Vec3f(1.f, 1.f, 1.f), 1.f);
        }


        part_sys->enable(PS_SIZE, PS_DISTANCE, PS_NEGATIVE);
        part_sys->enable(PS_BRIGTHNESS, PS_DISTANCE, PS_POSITIVE);
        part_sys->enable(PS_REDCOLOR, PS_LIFETIME, PS_NEGATIVE);
        part_sys->enable(PS_BLUECOLOR, PS_SPEED_COND, PS_NEGATIVE);
        part_sys->enable(PS_ALPHA, PS_LIFETIME, PS_NEGATIVE);
        part_sys->enable(PS_SPEED, PS_DISTANCE, PS_NEGATIVE);
        part_sys->enable(PS_SPEED, PS_LIFETIME, PS_POSITIVE);
    }

    createLights();

    /* tell GLUT to wait for events */
    glutMainLoop();
}
