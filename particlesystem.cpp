#define _USE_MATH_DEFINES
#include "materialcreator.h"
#include "particlesystem.h"
#include <math.h>
#include <random>
#include <chrono>
#include <algorithm>
#include <QDebug>

bool ParticleSystem::running = false;
bool ParticleSystem::updatePSFlag = false;

ParticleSystem::ParticleSystem(int _particleCount,
                               int _lifeTime,
                               int _stepTimeMS,
                               int _traceLength,
                               float _defSpeedCoef,
                               float _size):
    particleCount(_particleCount),
    lifeTime(_lifeTime),
    stepTimeMS(_stepTimeMS),
    traceLength(_traceLength),
    defSpeedCoef(_defSpeedCoef),
    defSizeCoef(_size),

    leftToGenerate(_particleCount),
    EPSILON(3/_defSpeedCoef),

    objectList(new std::vector<PSObject*>()),
    generators(new std::vector<PSObject*>()),
    displayLists(new std::vector<DisplayList>()),
    particles(new std::vector<Particle*>()),
    runtimeChanges(new std::vector<RuntimeChange>)
{
    quadric = gluNewQuadric();
    gluQuadricDrawStyle(quadric, GLU_FILL);
    gluQuadricTexture(quadric, GL_TRUE);
    gluQuadricNormals(quadric, GLU_SMOOTH);

    displayLists->push_back(DisplayList(glGenLists(1), 0));
    glNewList((*displayLists)[0].listNum,GL_COMPILE);
    glColor3d(1,1,1);
    gluSphere(quadric, 0.01, 4, 4);
    glEndList();

}

void ParticleSystem::generateObject(PSObject* _object, bool isWired,
                                    PART_MATERIAL _material,
                                    Vec3f _color,
                                    float _alpha,
                                    bool isDisplayable)
{
    switch (_material)
    {
    case PART_PLASTIC: _object->material = MaterialCreator::createPlasticMaterial(_color, _alpha); break;
    case PART_RUBBER: _object->material = MaterialCreator::createRubberMaterial(_color, _alpha); break;
    case PART_GOLD: _object->material = MaterialCreator::createGoldMaterial(_alpha); break;
    case PART_SILVER: _object->material = MaterialCreator::createSilverMaterial(_alpha); break;
    case PART_EMERALD: _object->material = MaterialCreator::createEmeraldMaterial(_alpha); break;
    default: _object->material = MaterialCreator::createPlasticMaterial(_color, _alpha); break;
    }
    _object->materialPart = _material;
    _object->colorPart = _color;
    _object->alpha = _alpha;
    // расчет матрицы трансляции
    {
        float c = cos(_object->rotationAngle * M_PI / 180);
        float s = sin(_object->rotationAngle * M_PI / 180);
        float magnitude = _object->rotation.magnitude();
        if((std::abs(magnitude - 1)>0.001)&&(magnitude > 0.00001))
            _object->rotation = _object->rotation.normalize();
        _object->rotationMatrix->push_back(
                    Vec3f((_object->rotation[0] * _object->rotation[0])*(1 - c) + c,
                (_object->rotation[0] * _object->rotation[1])*(1 - c) -
                (_object->rotation[2] * s),
                (_object->rotation[0] * _object->rotation[2]) * (1 - c) +
                (_object->rotation[1]) * s));
        _object->rotationMatrix->push_back(
                    Vec3f((_object->rotation[1] * _object->rotation[0])*(1 - c) +
                (_object->rotation[2] * s),
                (_object->rotation[1] * _object->rotation[1])*(1 - c) + c,
                (_object->rotation[1] * _object->rotation[2]) * (1 - c) -
                (_object->rotation[0]) * s));
        _object->rotationMatrix->push_back(
                    Vec3f((_object->rotation[0] * _object->rotation[2])*(1 - c) -
                (_object->rotation[1] * s),
                (_object->rotation[1] * _object->rotation[2])*(1 - c) +
                (_object->rotation[0] * s),
                (_object->rotation[2] *_object->rotation[2]) * (1 - c) + c));
        c = cos(_object->rotationAngle * -1.f * M_PI / 180);
        s = sin(_object->rotationAngle * -1.f * M_PI / 180);
        _object->antiRotationMatrix->push_back(
                    Vec3f((_object->rotation[0] * _object->rotation[0])*(1 - c) + c,
                (_object->rotation[0] * _object->rotation[1])*(1 - c) -
                (_object->rotation[2] * s),
                (_object->rotation[0] * _object->rotation[2]) * (1 - c) +
                (_object->rotation[1]) * s));
        _object->antiRotationMatrix->push_back(
                    Vec3f((_object->rotation[1] * _object->rotation[0])*(1 - c) +
                (_object->rotation[2] * s),
                (_object->rotation[1] * _object->rotation[1])*(1 - c) + c,
                (_object->rotation[1] * _object->rotation[2]) * (1 - c) -
                (_object->rotation[0]) * s));
        _object->antiRotationMatrix->push_back(
                    Vec3f((_object->rotation[0] * _object->rotation[2])*(1 - c) -
                (_object->rotation[1] * s),
                (_object->rotation[1] * _object->rotation[2])*(1 - c) +
                (_object->rotation[0] * s),
                (_object->rotation[2] *_object->rotation[2]) * (1 - c) + c));
    }
    _object->points->clear();
    switch (_object->type)
    {
    case PS_POINT:
    {
        _object->points->push_back(Vec3f(0,0,0));
        break;
    }
    case PS_PLANE:
    {
        float length = _object->settings[0];
        float height = _object->settings[1];
        float slices = _object->settings[3];
        float stacks = _object->settings[4];

        float dx = length / slices;
        float dy = height / stacks;
        float x = - (length / 2);
        float y = - (height / 2);
        for (int i =0; i <= slices; ++i)
        {
            for (int j =0; j <= stacks; ++j)
            {
                _object->points->push_back(Vec3f(x, y  , 0.0f ));
                x+= dx;
            }
            y+= dy;
            dx = - dx;
            x += dx;
        }

        Vec3f p0 = calculateWorldCoords(_object->points->at(0),
                                        (*_object->rotationMatrix),
                                        _object->coords);
        Vec3f p1 = calculateWorldCoords(_object->points->at(slices),
                                        (*_object->rotationMatrix),
                                        _object->coords);
        Vec3f p2 = calculateWorldCoords(_object->points->back(),
                                        (*_object->rotationMatrix),
                                        _object->coords);
        float A = (p1[1] * p2[2]) - (p1[1]* p0[2]) - (p0[1] * p2[2]) -
                (p1[2] * p2[1]) + (p1[2] * p0[1]) + (p0[2] * p2[1]);
        float B = (p2[0] * p1[2]) - (p0[0]* p1[2]) - (p2[0] * p0[2]) -
                (p2[2] * p1[0]) + (p2[2] * p0[0]) + (p0[2] * p1[0]);
        float C = (p1[0] * p2[1]) - (p0[0]* p2[1]) - (p1[0] * p0[1]) -
                (p2[0] * p1[1]) + (p0[0] * p1[1]) + (p2[0] * p0[1]);
        float D = (p1[0] * p0[1] * p2[2]) + (p0[0] * p2[1] * p1[2]) +
                (p2[0] * p1[1] * p0[2]) - (p1[0] * p2[1] * p0[2]) -
                (p2[0] * p0[1] * p1[2]) - (p0[0] * p1[1] * p2[2]);
        // ax+ by +cz +d = 0;
        _object->equation->push_back(A);
        _object->equation->push_back(B);
        _object->equation->push_back(C);
        _object->equation->push_back(D);
        _object->equationNormal = Vec3f(A,B,C);

        break;
    }
    case PS_SPHERE:
    {
        float radius = _object->settings[0];
        int slices = (int)_object->settings[3];
        int stacks = (int)_object->settings[4];
        float x, y, z;
        float rho, drho, theta, dtheta;
        float s, t, ds, dt;
        int i, j, imin, imax;
        drho = (float)M_PI / stacks;
        dtheta = (float)M_PI * 2.f/ slices;
        // draw +Z end as a triangle fan
        //glBegin(GL_TRIANGLE_FAN);
        _object->points->push_back(Vec3f(0.0f, 0.0f, radius));
        for (j = 0; j <= (slices - 1); j++) {
            theta = j * dtheta;
            x = -sin(theta) * sin(drho);
            y = cos(theta) * sin(drho);
            z = cos(drho);
            _object->points->push_back(Vec3f(x * radius, y * radius, z * radius));
        }
        //glEnd();


        ds = 1.0f / slices;
        dt = 1.0f / stacks;
        t = 1.0f; // because loop now runs from 0
        imin = 1;
        imax = stacks - 1;

        // draw intermediate stacks as quad strips
        for (i = imin; i < imax; i++) {
            rho = i * drho;
            //glBegin(GL_QUAD_STRIP);
            s = 0.0f;
            for (j = 0; j <= (slices - 1); j++) {
                theta = (j) * dtheta;
                x = -sin(theta) * sin(rho);
                y = cos(theta) * sin(rho);
                z = cos(rho);
                _object->points->push_back(Vec3f(x * radius, y * radius, z * radius));
                s += ds;
            }
            //glEnd();
            t -= dt;
        }
        // draw -Z end as a triangle fan
        //glBegin(GL_TRIANGLE_FAN);
        rho = M_PI - drho;
        s = 1.0f;
        _object->points->push_back(Vec3f(0.0f, 0.0f, -radius));
        for (j = slices - 1; j >= 0; j--) {
            theta = j * dtheta;;
            x = -sin(theta) * sin(rho);
            y = cos(theta) * sin(rho);
            z = cos(rho);
            _object->points->push_back(Vec3f(x * radius, y * radius, z * radius));
        }
        //glEnd();

        // нормаль - коорд точки - центр
        _object->equationNormal = calculateWorldCoords(Vec3f(0.f,0.f,0.f),
                                                       (*_object->rotationMatrix),
                                                       _object->coords);
         // (x-a)^2+(y-b)^2 + (z-c)^2 = R^2
        _object->equation->push_back(_object->equationNormal[0]);
        _object->equation->push_back(_object->equationNormal[1]);
        _object->equation->push_back(_object->equationNormal[2]);
        _object->equation->push_back(radius * radius);

        break;
    }
    case PS_CUBE:
    {
        float radius = _object->settings[0];
        float slices = _object->settings[3];
        float stacks = _object->settings[4];

        Vec3f planeCoord = Vec3f(0.f,0.f, radius);
        float settings [5] = {2.f *radius, 2.f*radius, 0.f, slices, stacks};
        generateObject(new PSObject(PS_PLANE,_object->function,
                                    Vec3f(0.f,0.f, radius), Vec3f(0.0f, 0.0f, 0.0f), 0.f,
                                    settings), true, _material,_color, _alpha, false);
        generateObject(new PSObject(PS_PLANE,_object->function,
                                    planeCoord, Vec3f(1.0f, 0.0f, 0.0f), 180.f,
                                    settings), true, _material,_color, _alpha, false);
        generateObject(new PSObject(PS_PLANE,_object->function,
                                    planeCoord, Vec3f(1.0f, 0.0f, 0.0f), 90.f,
                                    settings), true, _material,_color, _alpha, false);
        generateObject(new PSObject(PS_PLANE,_object->function,
                                    planeCoord, Vec3f(1.0f, 0.0f, 0.0f), -90.f,
                                    settings), true, _material,_color, _alpha, false);
        generateObject(new PSObject(PS_PLANE,_object->function,
                                    planeCoord, Vec3f(0.0f, 1.0f, 0.0f), 90.f,
                                    settings), true, _material,_color, _alpha, false);
        generateObject(new PSObject(PS_PLANE,_object->function,
                                    planeCoord, Vec3f(0.0f, 1.0f, 0.0f), -90.f,
                                    settings), true, _material,_color, _alpha, false);
        for (auto it = objectList->end() - 1;  it >= objectList->end() - 6 ; --it)
        {
            Vec3f p0 = calculateWorldCoords((*it)->points->at(0),
                                            (*_object->rotationMatrix),
                                            _object->coords);
            Vec3f p1 = calculateWorldCoords((*it)->points->at((int)slices),
                                            (*_object->rotationMatrix),
                                            _object->coords);
            Vec3f p2 = calculateWorldCoords((*it)->points->back(),
                                            (*_object->rotationMatrix),
                                            _object->coords);
            float A = (p1[1] * p2[2]) - (p1[1]* p0[2]) - (p0[1] * p2[2]) -
                    (p1[2] * p2[1]) + (p1[2] * p0[1]) + (p0[2] * p2[1]);
            float B = (p2[0] * p1[2]) - (p0[0]* p1[2]) - (p2[0] * p0[2]) -
                    (p2[2] * p1[0]) + (p2[2] * p0[0]) + (p0[2] * p1[0]);
            float C = (p1[0] * p2[1]) - (p0[0]* p2[1]) - (p1[0] * p0[1]) -
                    (p2[0] * p1[1]) + (p0[0] * p1[1]) + (p2[0] * p0[1]);
            float D = (p1[0] * p0[1] * p2[2]) + (p0[0] * p2[1] * p1[2]) +
                    (p2[0] * p1[1] * p0[2]) - (p1[0] * p2[1] * p0[2]) -
                    (p2[0] * p0[1] * p1[2]) - (p0[0] * p1[1] * p2[2]);
            // ax+ by +cz +d = 0;
            (*it)->equation->clear();
            (*it)->equation->push_back(A);
            (*it)->equation->push_back(B);
            (*it)->equation->push_back(C);
            (*it)->equation->push_back(D);
            (*it)->equationNormal = Vec3f(A,B,C);
        }
        break;
    }
    case PS_CONE:
        _object->settings[1] = 0.0f;
    case PS_CYLINDER:
    {
        float baseRadius = _object->settings[0];
        float topRadius = _object->settings[1];
        float height = _object->settings[2];
        int slices = (int)_object->settings[3];
        int stacks = (int)_object->settings[4];

        int _stacks = stacks;

        float da, r, dr, dz;
        float x, y, z;
        int i, j;
        da = (float)M_PI * 2.f/ slices;
        dr = (float)(topRadius - baseRadius) / _stacks;
        dz = (float)height / (_stacks);
        z = 0.0f;
        r = baseRadius;
        //glEnd();
        for (j = _stacks; j >= 0; --j) {
            for (i = slices ; i >= 1; --i) {
                x = sin((i * da));
                y = cos((i * da));
                _object->points->push_back(Vec3f((x * r), (y * r), z));
            } // for slices
            r += dr;
            z += dz;
        } // for _stacks
        // radius, real height, height of the cone top
        // cone with radius r and height h equation -h/r*sqrt(x*x + y*y) + h
        // cut conus -h/r*sqrt(x*x + y*y) + h where h top of the real conus
        // cylinder x*x + y*y = r * r
        Vec3f coordsDistribution = calculateWorldCoords(
                    Vec3f(1.f, 1.f, 0.f),
                    (*_object->rotationMatrix),
                    Vec3f(0.f, 0.f, 0.f));
        _object->equation->push_back(coordsDistribution[0]);
        _object->equation->push_back(coordsDistribution[1]);
        _object->equation->push_back(coordsDistribution[2]);
        coordsDistribution = calculateWorldCoords(
                    Vec3f(0.f, 0.f, 0.f),
                    (*_object->rotationMatrix),
                    _object->coords);
        _object->equation->push_back(coordsDistribution[0]);
        _object->equation->push_back(coordsDistribution[1]);
        _object->equation->push_back(coordsDistribution[2]);
        _object->equation->push_back(height);
        _object->equation->push_back(baseRadius);
        _object->equation->push_back(topRadius);
        _object->equationNormal = calculateWorldCoords(Vec3f(0.f, 0.f, (baseRadius - topRadius) / height),
                                                       (*_object->rotationMatrix),
                                                       Vec3f(0.f, 0.f, 0.f));

        break;
    }
    default: break;
    }
    if (isDisplayable)
    {
        displayLists->push_back(DisplayList(glGenLists(1), objectList->size()));
        glNewList((*displayLists)[displayLists->size()-1].listNum,GL_COMPILE);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, new float[4]{_object->material[0][0],
                    _object->material[0][1],
                    _object->material[0][2],
                    _object->material[0][3]}); // цвет кубика
        glMaterialfv(GL_FRONT, GL_SPECULAR, new float[4]{_object->material[1][0],
                    _object->material[1][1],
                    _object->material[1][2],
                    _object->material[1][3]}); // отраженный свет
        glMaterialfv(GL_FRONT, GL_AMBIENT, new float[4]{_object->material[2][0],
                    _object->material[2][1],
                    _object->material[2][2],
                    _object->material[2][3]});
        glMaterialfv(GL_FRONT, GL_SHININESS, new float[1]{_object->material[3][0]});
        switch(_object->type)
        {
        case PS_PLANE:
            if (isWired)
                glBegin(GL_LINE_LOOP);
            else
                glBegin(GL_QUADS);
             glVertex3f(-_object->settings[0]/2.f, -_object->settings[1]/2.f, 0.f);
             glVertex3f(_object->settings[0]/2.f, -_object->settings[1]/2.f, 0.f);
             glVertex3f(_object->settings[0]/2.f, _object->settings[1]/2.f, 0.f);
             glVertex3f(-_object->settings[0]/2.f, _object->settings[1]/2.f, 0.f);
            glEnd();
            break;
        case PS_SPHERE:
            if (isWired)
                gluQuadricDrawStyle(quadric, GLU_LINE);
            else
                gluQuadricDrawStyle(quadric, GLU_FILL);
            gluSphere(quadric,_object->settings[0], _object->settings[3],
                    _object->settings[4]);
            break;
        case PS_CUBE:
            if (isWired)
                glutWireCube(_object->settings[0] * 2);
            else
                glutSolidCube(_object->settings[0] * 2);
            break;
        case PS_CONE:
        case PS_CYLINDER:
            if (isWired)
                gluQuadricDrawStyle(quadric, GLU_LINE);
            else
                gluQuadricDrawStyle(quadric, GLU_FILL);
            gluCylinder(quadric, _object->settings[0], _object->settings[1],
                    _object->settings[2], _object->settings[3], _object->settings[4]);
            break;
        default: break;
        }

        glPointSize(5.f);
        glBegin(GL_POINTS);
        {
            for (unsigned i = 0; i < _object->points->size(); i++)
            {

                glVertex3f(_object->points->at(i)[0], _object->points->at(i)[1],
                        _object->points->at(i)[2]);
                if ((*_object->points)[i][0] < _object->minCoords[0])
                    _object->minCoords[0] = (*_object->points)[i][0];
                if ((*_object->points)[i][1] < _object->minCoords[1])
                    _object->minCoords[1] = (*_object->points)[i][1];
                if ((*_object->points)[i][2] < _object->minCoords[2])
                    _object->minCoords[2] = (*_object->points)[i][2];
                if ((*_object->points)[i][0] > _object->maxCoords[0])
                    _object->maxCoords[0] = (*_object->points)[i][0];
                if ((*_object->points)[i][1] > _object->maxCoords[1])
                    _object->maxCoords[1] = (*_object->points)[i][1];
                if ((*_object->points)[i][2] > _object->maxCoords[2])
                    _object->maxCoords[2] = (*_object->points)[i][2];
                _object->points->at(i) = calculateWorldCoords(_object->points->at(i),
                                                              (*_object->rotationMatrix),
                                                              _object->coords);
            }
        }
        glEnd();
        glEndList();
    }
    else
    {
        for (unsigned i = 0; i < _object->points->size(); i++)
        {
            if ((*_object->points)[i][0] < _object->minCoords[0])
                _object->minCoords[0] = (*_object->points)[i][0];
            if ((*_object->points)[i][1] < _object->minCoords[1])
                _object->minCoords[1] = (*_object->points)[i][1];
            if ((*_object->points)[i][2] < _object->minCoords[2])
                _object->minCoords[2] = (*_object->points)[i][2];
            if ((*_object->points)[i][0] > _object->maxCoords[0])
                _object->maxCoords[0] = (*_object->points)[i][0];
            if ((*_object->points)[i][1] > _object->maxCoords[1])
                _object->maxCoords[1] = (*_object->points)[i][1];
            if ((*_object->points)[i][2] > _object->maxCoords[2])
                _object->maxCoords[2] = (*_object->points)[i][2];
            _object->points->at(i) = calculateWorldCoords(_object->points->at(i),
                                                          (*_object->rotationMatrix),
                                                          _object->coords);
        }
    }
    switch (_object->function)
    {
    case PS_GENERATOR:
        if(_object->type != PS_CUBE)
        generators->push_back(_object); break;
        default: break;
    }
    objectList->push_back(_object);
}


void ParticleSystem::initScene()
{

    for(unsigned i = 1; i< displayLists->size(); ++i)
    {
        glPushMatrix();
        glMaterialfv(GL_FRONT, GL_DIFFUSE, new float[4]{
            objectList->at((*displayLists)[i].objNum)->material[0][0],
                    objectList->at((*displayLists)[i].objNum)->material[0][1],
                    objectList->at((*displayLists)[i].objNum)->material[0][2],
                    objectList->at((*displayLists)[i].objNum)->material[0][3]}); // цвет кубика
        glMaterialfv(GL_FRONT, GL_SPECULAR, new float[4]{
            objectList->at((*displayLists)[i].objNum)->material[1][0],
                    objectList->at((*displayLists)[i].objNum)->material[1][1],
                    objectList->at((*displayLists)[i].objNum)->material[1][2],
                    objectList->at((*displayLists)[i].objNum)->material[1][3]}); // отраженный свет
        glMaterialfv(GL_FRONT, GL_AMBIENT, new float[4]{
            objectList->at((*displayLists)[i].objNum)->material[2][0],
                    objectList->at((*displayLists)[i].objNum)->material[2][1],
                    objectList->at((*displayLists)[i].objNum)->material[2][2],
                    objectList->at((*displayLists)[i].objNum)->material[2][3]});
        glMaterialfv(GL_FRONT, GL_SHININESS, new float[1]{
            objectList->at((*displayLists)[i].objNum)->material[3][0]});
        glRotatef(objectList->at((*displayLists)[i].objNum)->rotationAngle,
                  objectList->at((*displayLists)[i].objNum)->rotation[0],
                objectList->at((*displayLists)[i].objNum)->rotation[1],
                objectList->at((*displayLists)[i].objNum)->rotation[2]);
        glTranslatef(objectList->at((*displayLists)[i].objNum)->coords[0],
                objectList->at((*displayLists)[i].objNum)->coords[1],
                objectList->at((*displayLists)[i].objNum)->coords[2]);
        glCallList((*displayLists)[i].listNum);
        glPopMatrix();
    }
    for (auto it = particles->begin(); it< particles->end(); ++it)
    {
        glMaterialfv(GL_FRONT, GL_SPECULAR, new float[4]{
            (*it)->getMaterial()[1][0],
                    (*it)->getMaterial()[1][1],
                    (*it)->getMaterial()[1][2],
                    (*it)->getMaterial()[1][3]}); // отраженный свет
        glMaterialfv(GL_FRONT, GL_AMBIENT, new float[4]{
            (*it)->getMaterial()[2][0],
                    (*it)->getMaterial()[2][1],
                    (*it)->getMaterial()[2][2],
                    (*it)->getMaterial()[2][3]});
        glMaterialfv(GL_FRONT, GL_SHININESS, new float[1]{
            (*it)->getMaterial()[3][0]});

        glPushMatrix();
        glTranslated((*it)->getCoords()[0],(*it)->getCoords()[1],
                (*it)->getCoords()[2]);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, new float[4]{
            (*it)->getMaterial()[0][0],
                    (*it)->getMaterial()[0][1],
                    (*it)->getMaterial()[0][2],
                    (*it)->getMaterial()[0][3]}); // цвет кубика
        glPushMatrix();
        glScalef((*it)->getSize(),(*it)->getSize(),(*it)->getSize());
        glCallList((*displayLists)[0].listNum);
        glPopMatrix();
        glPopMatrix();

        if ((*it)->getTrace()->size() > 0)
        {
            float _alpha = (*it)->getMaterial()[0][3] / 2.0f;
            for (auto it2 = (*it)->getTrace()->begin(); it2 < (*it)->getTrace()->end(); ++it2)
            {
                glPushMatrix();
                glTranslated((*it2)[0],(*it2)[1],
                        (*it2)[2]);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, new float[4]{
                    (*it)->getMaterial()[0][0],
                            (*it)->getMaterial()[0][1],
                            (*it)->getMaterial()[0][2],
                            _alpha}); // цвет кубика
                glPushMatrix();
                glScalef((*it)->getSize(),(*it)->getSize(),(*it)->getSize());
                glCallList((*displayLists)[0].listNum);
                glPopMatrix();
                glPopMatrix();
                _alpha /= 2;
            }
        }
    }
}

int ParticleSystem::generateParticles(int particlesToGenerate)
{
    if (particlesToGenerate > 0)
    {
        int generated = 0;
        if (!generators->empty())
        {
            int genNumber = 0, pointNumber = 0;
            std::mt19937 generator(std::chrono::system_clock::now().
                                   time_since_epoch().count());
            std::uniform_int_distribution<int> distribution(0,generators->size()-1);
            std::vector<std::vector<int>> usedPoints(generators->size());
            for (auto i =0; i < particlesToGenerate; ++i)
            {
                if(generators->size() != 1)
                    genNumber = distribution(generator);
                else
                    genNumber = 0;
                if((*generators)[genNumber]->points->size() != 1)
                {
                    std::uniform_int_distribution<int> distribution2
                            (0,(*generators)[genNumber]->points->size() - 1);
                    pointNumber = distribution2(generator);
                }
                else
                    pointNumber = 0;
                //pointNumber = 32;
                if (std::find(usedPoints.at(genNumber).begin(),
                              usedPoints.at(genNumber).end(),pointNumber) ==
                        usedPoints.at(genNumber).end())
                {
                    Vec3f coords = (*(*generators)[genNumber]->points)[pointNumber];
                    // bug point with speed breakthrough with defailt cone
                    //coords = Vec3f(-8.1958495500000003e-10,-0.0093749593899999997, 0.37499991100000002);
                    Vec3f speed = (*generators)[genNumber]->equationNormal;
                    switch ((*generators)[genNumber]->type)
                    {
                    case PS_POINT:
                    {
                        std::uniform_real_distribution<float> distribution2
                                ((*generators)[genNumber]->settings[0],
                                (*generators)[genNumber]->settings[2]);
                        std::uniform_real_distribution<float> distribution3
                                ((*generators)[genNumber]->settings[1],
                                (*generators)[genNumber]->settings[3]);
                        std::uniform_real_distribution<float> distribution4
                                (-1.0f, (*generators)[genNumber]->settings[4]);
                        speed = Vec3f(distribution2(generator),
                                      distribution3(generator),
                                      distribution4(generator));
                        break;
                    }
                    case PS_SPHERE:
                    {
                        speed = coords - speed;
                        break;
                    }
                    case PS_CONE:
                    case PS_CYLINDER:
                    {
                        // TODO
                        // normals of cube and cylinder works correctly
                        // only when rotate on 0 or 90 applied on ONE axis
                        Vec3f temp;
                        temp[0] = (*(*generators)[genNumber]->equation)[0] * (speed[0] + coords[0] - (*(*generators)[genNumber]->equation)[3]);
                        temp[1] = (*(*generators)[genNumber]->equation)[1] * (speed[1] + coords[1] - (*(*generators)[genNumber]->equation)[4]);
                        temp[2] = (*(*generators)[genNumber]->equation)[2] * (speed[2] + coords[2] - (*(*generators)[genNumber]->equation)[5]);
                        Vec3f temp2;
                        temp2[0] = (1 - (*(*generators)[genNumber]->equation)[0]) * (speed[0]);
                        temp2[1] = (1 - (*(*generators)[genNumber]->equation)[1]) * (speed[1]);
                        temp2[2] = (1 - (*(*generators)[genNumber]->equation)[2]) * (speed[2]);
                        speed = temp.normalize();
                        speed += temp2;
                        break;
                    }
                    default: break;
                    }
                    speed = speed.normalize();
                    particles->push_back(new Particle(coords,
                                                      (*generators)[genNumber]->materialPart,
                                                      (*generators)[genNumber]->colorPart ,
                                                      (*generators)[genNumber]->alpha,
                                                      speed, lifeTime, traceLength,
                                                      IP_START, defSpeedCoef, defSizeCoef));
                    usedPoints.at(genNumber).push_back(pointNumber);
                    ++generated;
                }

            }
        }
        return particlesToGenerate - generated;
    }
    return 0;
}

void ParticleSystem::updateParticles()
{
    if (ParticleSystem::updatePSFlag)
    {
        int cubePlanes = 0;
        for (auto it = particles->begin(); it< particles->end(); ++it)
        {
            EPSILON = 0.02f /(*it)->getSpeedScale();
            std::vector<Vec3f> collisions;
            Vec3f coords, coords1, attractions = Vec3f(0.0f, 0.0f, 0.0f);
            for(auto it2 = objectList->begin() + (objectList->size() - 1); it2 >= objectList->begin(); --it2)
            {
                // localize current point
                if (cubePlanes == 0)
                {
                    coords = (*it)->getCoords();
                }
                else
                {
                    coords = calculateLocalCoords((*it)->getCoords(), (*(*(it2 + 7 - cubePlanes))->antiRotationMatrix),
                                                         (*(it2 + 7 - cubePlanes))->coords);
                }
                coords = calculateLocalCoords(coords, (*(*it2)->antiRotationMatrix),
                                                     (*it2)->coords);

                // ATTRACTION DETECTION
                if (((*it2)->function == PS_ATTRACTOR) ||
                        ((*it2)->function == PS_ANTIATTRACTOR))
                {
                    float normDirection = 1.0f;
                    if ((*it2)->function == PS_ATTRACTOR)
                        normDirection = -1.0f;
                    switch((*it2)->type)
                    {
                    case PS_POINT:
                    {
                        if ((0.16 > coords.magnitudeSquared()) &&
                                (0.04 < coords.magnitudeSquared()))
                            attractions += normDirection * ((*it)->getCoords() - (*it2)->coords) * (std::abs(0.2 - coords.magnitude())/0.1);
                        break;
                    }
                    case PS_PLANE:
                    {
                        Vec3f f = Vec3f(0.0f, 0.0f, 1.0f)*((((*it2)->maxCoords)[0]));
                        if ((coords > (((*it2)->minCoords - f ))) &&
                                (coords < ((*it2)->maxCoords + f )) &&
                                (coords[2] > 0))
                            attractions += normDirection * (*it2)->equationNormal * (std::abs(f[2] - coords[2])/f[2]);
                        break;
                    }
                    case PS_SPHERE:
                    {
                        if (((4.0f * (*(*it2)->equation)[3]) > coords.magnitudeSquared()) &&
                                (((*(*it2)->equation)[3]) < coords.magnitudeSquared()))
                            attractions += normDirection * ((*it)->getCoords() - (*it2)->equationNormal) * (std::abs(std::sqrt(4.0f * (*(*it2)->equation)[3]) - coords.magnitude())/std::sqrt((*(*it2)->equation)[3]));
                        break;
                    }
                    case PS_CONE:
                    {
                        // GO away, crazy SOB, it's, goddamn, impossible!
                        break;
                    }
                    case PS_CYLINDER:
                    {
                        if ((coords > ((*it2)->minCoords - Vec3f(std::abs((*it2)->minCoords[0]), std::abs((*it2)->minCoords[1]), 0))) &&
                             (coords < ((*it2)->maxCoords + Vec3f(std::abs((*it2)->maxCoords[0]), std::abs((*it2)->maxCoords[1]), 0))) &&
                                (std::abs(std::abs((*(*it2)->equation)[7]) -
                                          std::abs((*(*it2)->equation)[8])) <
                                 EPSILON))
                        {
                            if(((coords [0] * coords [0] + coords [1] * coords [1]) > ((*(*it2)->equation)[7] * (*(*it2)->equation)[7])) &&
                                    ((coords [0] * coords [0] + coords [1] * coords [1]) < (4.0f * (*(*it2)->equation)[7] * (*(*it2)->equation)[7])))
                            {
                                Vec3f temp;
                                temp[0] = (*(*it2)->equation)[0] * ((*it2)->equationNormal[0] + coords[0] - (*(*it2)->equation)[3]);
                                temp[1] = (*(*it2)->equation)[1] * ((*it2)->equationNormal[1] + coords[1] - (*(*it2)->equation)[4]);
                                temp[2] = (*(*it2)->equation)[2] * ((*it2)->equationNormal[2] + coords[2] - (*(*it2)->equation)[5]);
                                Vec3f temp2;
                                temp2[0] = (1 - (*(*it2)->equation)[0]) * ((*it2)->equationNormal[0]);
                                temp2[1] = (1 - (*(*it2)->equation)[1]) * ((*it2)->equationNormal[1]);
                                temp2[2] = (1 - (*(*it2)->equation)[2]) * ((*it2)->equationNormal[2]);
                                attractions += normDirection * (temp.normalize() + temp2) * (std::abs(std::sqrt(4.0f * (*(*it2)->equation)[7] * (*(*it2)->equation)[7]) - std::sqrt(coords [0] * coords [0] + coords [1] * coords [1]))/(*(*it2)->equation)[7]);
                            }
                        }
                        break;
                    }
                    default: break;
                    }
                }

                // point prediction
                if (cubePlanes == 0)
                {
                    coords1 = (*it)->getNext(attractions);
                }
                else
                {
                    coords1 = calculateLocalCoords(((*it)->getNext(attractions)), (*(*(it2 + 7 - cubePlanes))->antiRotationMatrix),
                                                         (*(it2 + 7 - cubePlanes))->coords);
                    --cubePlanes;
                }
                coords1 = calculateLocalCoords(coords1, (*(*it2)->antiRotationMatrix),
                                                     (*it2)->coords);

                // COLLISION DETECTION
                switch((*it2)->type)
                {
                case PS_PLANE:
                {
                    if ((coords > ((*it2)->minCoords)) &&
                            (coords < ((*it2)->maxCoords)))
                        if((coords[2] * coords1[2]) <= 0)
                            collisions.push_back((*it2)->equationNormal);
                    break;
                }
                case PS_CUBE:
                {
                    cubePlanes = 6;
                    break;
                }
                case PS_SPHERE:
                {
                    if (coords > ((*it2)->minCoords) &&
                            (coords < ((*it2)->maxCoords)))
                        if ((((*(*it2)->equation)[3] - coords.magnitudeSquared()) *
                             ((*(*it2)->equation)[3] - coords1.magnitudeSquared()))  < 0)
                            collisions.push_back((*it)->getCoords() - (*it2)->equationNormal);
                    break;
                }
                case PS_CONE:
                case PS_CYLINDER:
                {
                    if (coords > ((*it2)->minCoords) &&
                            (coords < ((*it2)->maxCoords)))
                    {
                        float z,z1;
                        if (std::abs(std::abs((*(*it2)->equation)[7]) -
                                     std::abs((*(*it2)->equation)[8])) > EPSILON)
                        {
                            // cone
                            float ctg = (*(*it2)->equation)[6] /
                                    (((*(*it2)->equation)[7]) - (*(*it2)->equation)[8]);
                            z = coords[2] - ((-1.0f) * ctg *
                                 std::sqrt((coords[0] * coords[0]) + (coords[1] * coords[1])) + ((*(*it2)->equation)[7] * ctg));
                            z1 =  coords[2] - ((-1.0f) * ctg *
                                   std::sqrt((coords1[0] * coords1[0]) + (coords1[1] * coords1[1]))) + ((*(*it2)->equation)[7] * ctg);
                        }
                        else
                        {
                            // cylinder
                            z = coords [0] * coords [0] + coords [1] * coords [1] -
                                    ((*(*it2)->equation)[7] * (*(*it2)->equation)[7]);
                            z1 = coords1 [0] * coords1 [0] + coords1 [1] * coords1 [1] -
                                    ((*(*it2)->equation)[7] * (*(*it2)->equation)[7]);

                        }
                        if((z*z1) < 0)
                        {
                            Vec3f temp;
                            temp[0] = (*(*it2)->equation)[0] * ((*it2)->equationNormal[0] + coords[0] - (*(*it2)->equation)[3]);
                            temp[1] = (*(*it2)->equation)[1] * ((*it2)->equationNormal[1] + coords[1] - (*(*it2)->equation)[4]);
                            temp[2] = (*(*it2)->equation)[2] * ((*it2)->equationNormal[2] + coords[2] - (*(*it2)->equation)[5]);
                            Vec3f temp2;
                            temp2[0] = (1 - (*(*it2)->equation)[0]) * ((*it2)->equationNormal[0]);
                            temp2[1] = (1 - (*(*it2)->equation)[1]) * ((*it2)->equationNormal[1]);
                            temp2[2] = (1 - (*(*it2)->equation)[2]) * ((*it2)->equationNormal[2]);
                            collisions.push_back(temp.normalize() + temp2);
                        }
                    }
                    break;
                }
                default: break;
                }
            }
            (*it)->handleCollision(collisions, attractions);
            PART_STATE state = PART_MOVED;
            state = (*it)->updateOnTimer();
            (*it)->calcRuntime(runtimeChanges);
            if(state == PART_TODESTROY)
            {
                particles->erase(it);
                ++leftToGenerate;
            }
        }
        ParticleSystem::updatePSFlag = false;
        leftToGenerate = generateParticles(leftToGenerate);
    }
}

void ParticleSystem::start()
{
    if (!running)
    {
        running = true;
        leftToGenerate = generateParticles(particleCount);
        glutTimerFunc(stepTimeMS, timerEvent, stepTimeMS);
    }
}

void ParticleSystem::timerEvent(int time)
{
    updatePSFlag = true;
    if (ParticleSystem::running)
        glutTimerFunc(time, timerEvent, time);
    glutPostRedisplay();
    return;
}

void ParticleSystem::stop()
{
    leftToGenerate = 0;
    ParticleSystem::running = false;
    particles->clear();
}

Vec3f ParticleSystem::calculateWorldCoords(Vec3f point, std::vector<Vec3f> rotationMatrix,
                                           Vec3f globalCoords)
{
    Vec3f result = point + globalCoords;
    result = Vec3f(result.dot(rotationMatrix[0]),
            result.dot(rotationMatrix[1]),
            result.dot(rotationMatrix[2]));
    return result;
}

Vec3f ParticleSystem::calculateLocalCoords(Vec3f point, std::vector<Vec3f> antiRotationMatrix,
                                           Vec3f globalCoords)
{
    Vec3f result = Vec3f(point.dot(antiRotationMatrix[0]),
            point.dot(antiRotationMatrix[1]),
            point.dot(antiRotationMatrix[2]));
    result = result - globalCoords;
    return result;
}

void ParticleSystem::enable(PS_CHANGEBLE_PARAM param, PS_CHANGE_CONDITION cond,
                            PS_RELATIONSHIP rel)
{
    RuntimeChange change(param, cond, rel);
    if (std::find(runtimeChanges->begin(),
                  runtimeChanges->end(), change) ==
            runtimeChanges->end())
        runtimeChanges->push_back(change);
}

void ParticleSystem::disable(PS_CHANGEBLE_PARAM param, PS_CHANGE_CONDITION cond,
                             PS_RELATIONSHIP rel)
{
    RuntimeChange change(param, cond, rel);
    auto it = std::find(runtimeChanges->begin(),runtimeChanges->end(), change);
    (*it).disabled = true;
}
