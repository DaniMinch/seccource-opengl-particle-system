#include "materialcreator.h"

std::vector<std::vector<float>> MaterialCreator::createPlasticMaterial(Vec3f _color, float _alpha)
{
    std::vector<std::vector<float>> material;
    std::vector<float> diffuse = {_color[0] / 2.f, _color[1] / 2.f, _color[2] / 2.f, _alpha};
    std::vector<float> specular = {diffuse[0]*1.25f, diffuse[0]*1.25f,
                         ((diffuse[0] + diffuse[1] + diffuse[2]) / 3.f)*1.15f, _alpha};
    std::vector<float> ambient = {0.0f, 0.0f, 0.0f, _alpha};
    std::vector<float> shininess = { 32.f };
    material.push_back(diffuse);
    material.push_back(specular);
    material.push_back(ambient);
    material.push_back(shininess);
    return material;
}

std::vector<std::vector<float>> MaterialCreator::createRubberMaterial(Vec3f _color, float _alpha)
{
    std::vector<std::vector<float>> material;
    std::vector<float> diffuse = {(_color[0]/10.f) + .4f,
                        (_color[1]/10.f) + .4f,
                        (_color[2]/10.f) + .4f, _alpha};
    std::vector<float> ambient = {_color[0]/20.f, _color[1]/20.f, _color[2]/20.f, _alpha};
    std::vector<float> specular = {_color[0], _color[1], _color[2], _alpha};
    for (auto i = 0; i < 3; ++i)
        specular[i] = 0 ? 0.04 : specular[i] * .7f;
    if (_color.magnitudeSquared() <= .15f)
    {
        specular[0] = .4f;
        specular[1] = .4f;
        specular[2] = .4f;
    }
    std::vector<float> shininess = { 10.f };
    material.push_back(diffuse);
    material.push_back(specular);
    material.push_back(ambient);
    material.push_back(shininess);
    return material;
}

std::vector<std::vector<float>> MaterialCreator::createGoldMaterial(float _alpha)
{
    std::vector<std::vector<float>> material;
    std::vector<float> diffuse = {0.75164f, 0.60648f, 0.2648f, _alpha};
    std::vector<float> specular = {0.628281f, .555802f, .366065f, _alpha};
    std::vector<float> ambient = {0.24725f, 0.1995f, 0.0745f, _alpha};
    std::vector<float> shininess = { 51.2f };
    material.push_back(diffuse);
    material.push_back(specular);
    material.push_back(ambient);
    material.push_back(shininess);
    return material;
}

std::vector<std::vector<float>> MaterialCreator::createSilverMaterial(float _alpha)
{
    std::vector<std::vector<float>> material;
    std::vector<float> diffuse = {0.50754f, 0.50754f, 0.50754f, _alpha};
    std::vector<float> specular = {0.508273f, 0.508273f, 0.508273f, _alpha};
    std::vector<float> ambient = {0.19225f, 0.19225f, 0.19225f, _alpha};
    std::vector<float> shininess = { 51.2f };
    material.push_back(diffuse);
    material.push_back(specular);
    material.push_back(ambient);
    material.push_back(shininess);
    return material;
}

std::vector<std::vector<float>> MaterialCreator::createEmeraldMaterial(float _alpha)
{
    std::vector<std::vector<float>> material;
    std::vector<float> diffuse = {0.07568f, 0.61424f, 0.07568f, _alpha};
    std::vector<float> specular = {0.633f, 0.727811f, 0.633f, _alpha};
    std::vector<float> ambient = {0.0215f, 0.1745f, 0.0215f, _alpha};
    std::vector<float> shininess = { 76.8f };
    material.push_back(diffuse);
    material.push_back(specular);
    material.push_back(ambient);
    material.push_back(shininess);
    return material;
}

