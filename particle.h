#ifndef PARTICLE_H
#define PARTICLE_H

#include <deque>
#include "vec3f.h" // вектор 3 флоатов
#include <vector>

enum PART_MATERIAL{
    PART_PLASTIC,
    PART_RUBBER,
    PART_GOLD,
    PART_EMERALD,
    PART_SILVER
};

enum PART_STATE{
    PART_MOVED,
    PART_TODESTROY
};

enum IP_TYPE{
    IP_START,
    IP_COLLISION
};

enum PS_CHANGEBLE_PARAM{
    PS_SIZE,
    PS_SPEED,
    PS_BRIGTHNESS,
    PS_REDCOLOR,
    PS_GREENCOLOR,
    PS_BLUECOLOR,
    PS_ALPHA
};

enum PS_CHANGE_CONDITION{
    PS_DISTANCE,
    PS_LIFETIME,
    PS_SPEED_COND
};

enum PS_RELATIONSHIP{
    PS_POSITIVE,
    PS_NEGATIVE
};

struct RuntimeChange{
    RuntimeChange(PS_CHANGEBLE_PARAM _param, PS_CHANGE_CONDITION _cond, PS_RELATIONSHIP _rel):
    param(_param), cond (_cond), rel(_rel), disabled(false){}
    PS_CHANGEBLE_PARAM param;
    PS_CHANGE_CONDITION cond;
    PS_RELATIONSHIP rel;
    bool disabled;
    bool operator==(const RuntimeChange &other)
    {return ((param == other.param) && (cond == other.cond) && (rel == other.rel));}
};

class Particle
{
public:

    explicit Particle(Vec3f _coords = Vec3f(0.f, 0.f, 0.f),
                      PART_MATERIAL _material = PART_PLASTIC,
                      Vec3f _color = Vec3f(0.f, 0.f, 0.f),
                      float _alpha = 1.f,
                      Vec3f _speed = Vec3f(1.f, 0.f, 0.f),
                      int _lifetime = 200,
                      int _traceLength = 0,
                      IP_TYPE _ip_type = IP_START,
                      int _defSpeedCoef = 200,
                      float _defSize = 1.f);
    // ~Particle();

    PART_STATE updateOnTimer();
    bool handleCollision(std::vector<Vec3f> collisions, Vec3f attractions);
    const Vec3f getCoords(){return coords;}
    const Vec3f getSpeed(){return speed;}
    const Vec3f getNext(Vec3f attraction);
    std::vector<std::vector<float>> getMaterial(){ return material;}
    void calcRuntime(std::vector<RuntimeChange> *changes);
    float getSpeedScale(){return speedScaleCoef/defSpeedScaleCoef;}
    float getSize(){return size;}
    std::deque<Vec3f>* getTrace(){return trace;}


private:
    Vec3f coords;
    Vec3f speed;
    int lifeTime;
    int traceLength;

    IP_TYPE ip_type;
    const float defSpeedScaleCoef;
    const float defSize;
    int defLifetime;
    float speedScaleCoef;
    float size;
    Vec3f importantPoint;
    std::deque<Vec3f>* trace;
    std::vector<std::vector<float>> material;
    std::vector<std::vector<float>> defMaterial;
};

#endif // PARTICLE_H
