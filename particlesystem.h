#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include "particle.h"

#include <GL/freeglut.h>

enum PS_OBJECT_TYPE
{
    PS_POINT,
    PS_PLANE, // length, width, slices, stacks
    PS_SPHERE, // radius, slices, stacks
    PS_CUBE, //radius, slices, stacks
    PS_CONE, //base radius, top radius = 0, height, slices, stacks
    PS_CYLINDER //base radius, top radius, height, slices, stacks
};

enum PS_FUNCTION
{
    PS_GENERATOR,
    PS_SURFACE,
    PS_ATTRACTOR,
    PS_ANTIATTRACTOR
};

struct PSObject
{
    PSObject(PS_OBJECT_TYPE _type,
             PS_FUNCTION _function,
             Vec3f _coords,
             Vec3f _rotation,
             float _rotAngle,
             float _formsettings[])
        :type(_type), function(_function),coords(_coords), rotation(_rotation),
          settings(_formsettings), rotationAngle(_rotAngle),
          points(new std::vector<Vec3f>),
          rotationMatrix(new std::vector<Vec3f>),
          antiRotationMatrix(new std::vector<Vec3f>),
          equation(new std::vector<float>),
          minCoords(Vec3f(10000.f, 10000.f, 10000.f)),
          maxCoords(Vec3f(-10000.f, -10000.f, -10000.f)){}
    PS_OBJECT_TYPE type;
    PS_FUNCTION function;
    Vec3f coords;
    Vec3f rotation;
    float* settings;
    float rotationAngle;
    std::vector<std::vector<float>> material;
    std::vector<Vec3f>* points;
    std::vector<Vec3f>* rotationMatrix;
    std::vector<Vec3f>* antiRotationMatrix;
    std::vector<float>* equation;
    Vec3f minCoords;
    Vec3f maxCoords;
    Vec3f equationNormal;

    PART_MATERIAL materialPart;
    float alpha;
    Vec3f colorPart;
};

struct DisplayList
{
    DisplayList(GLuint list,int obj):listNum(list), objNum(obj){}
    GLuint listNum;
    int objNum;
};

class ParticleSystem
{
public:
    ParticleSystem(int _particleCount = 200,
                   int _lifeTime  = 3,
                   int _stepTimeMS = 50,
                   int _traceLength = 0,
                   float _defSpeedCoef = 200,
                   float _size = 1.f);
    void generateObject(PSObject *_object,
                        bool isWired,
                        PART_MATERIAL _material,
                        Vec3f _color,
                        float _alpha,
                        bool isDisplayable = true);
    void updateParticles();
    bool isRunning(){return running;}
    bool getUpdateFlag(){return ParticleSystem::updatePSFlag;}
    int getStepTime(){return stepTimeMS;}
    void setStepTime(unsigned int time){stepTimeMS = time;}
    void enable(PS_CHANGEBLE_PARAM param, PS_CHANGE_CONDITION cond, PS_RELATIONSHIP rel);
    void disable(PS_CHANGEBLE_PARAM param, PS_CHANGE_CONDITION cond, PS_RELATIONSHIP rel);
    void initScene();
    void start();
    void stop();


private:
    int particleCount;
    int lifeTime;
    unsigned int stepTimeMS;
    int traceLength;
    float defSpeedCoef;
    float defSizeCoef;
    GLUquadricObj* quadric;
    int leftToGenerate;
    float EPSILON;

    static bool updatePSFlag;
    static bool running;

    std::vector<PSObject*>* objectList;
    std::vector<PSObject*>* generators;

    std::vector<DisplayList>* displayLists;

    std::vector<Particle*>* particles;

    std::vector<RuntimeChange>* runtimeChanges;

    int generateParticles(int particlesToGenerate);
    static void timerEvent(int time);

    Vec3f calculateWorldCoords(const Vec3f point,
                               const std::vector<Vec3f> rotationMatrix,
                               const Vec3f globalCoords);

    Vec3f calculateLocalCoords(const Vec3f point,
                               const std::vector<Vec3f> rotationMatrix,
                               const Vec3f globalCoords);
};

#endif // PARTICLESYSTEM_H
