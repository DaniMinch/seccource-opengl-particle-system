#include "particle.h"
#include "materialcreator.h"
#include <algorithm>

Particle::Particle(Vec3f _coords,
                   PART_MATERIAL _material,
                   Vec3f _color,
                   float _alpha,
                   Vec3f _speed,
                   int _lifetime,
                   int _traceLength,
                   IP_TYPE _ip_type,
                   int _defSpeedCoef,
                   float _defSize):
    coords(_coords),
    speed(_speed),
    lifeTime(_lifetime),
    traceLength(_traceLength),
    ip_type(_ip_type),
    defSpeedScaleCoef(_defSpeedCoef),
    defSize(_defSize),

    defLifetime(_lifetime),
    speedScaleCoef(_defSpeedCoef),
    size(_defSize),
    importantPoint(_coords),
    trace(new std::deque<Vec3f>)
{
    switch (_material)
    {
    case PART_PLASTIC: material = MaterialCreator::createPlasticMaterial(_color, _alpha); break;
    case PART_RUBBER: material = MaterialCreator::createRubberMaterial(_color, _alpha); break;
    case PART_GOLD: material = MaterialCreator::createGoldMaterial(_alpha); break;
    case PART_SILVER: material = MaterialCreator::createSilverMaterial(_alpha); break;
    case PART_EMERALD: material = MaterialCreator::createEmeraldMaterial(_alpha); break;
    default: material = MaterialCreator::createPlasticMaterial(_color, _alpha); break;
    }
    defMaterial = material;
    for(auto i = 0; i< traceLength; ++i)
        trace->push_back(coords);
    updateOnTimer();
}

PART_STATE Particle::updateOnTimer()
{
    if (--lifeTime == 0)
    {
        return PART_TODESTROY;
    }
    else
    {
        // recalc coords and traces
        coords = coords + (speed / speedScaleCoef);
        if (traceLength > 0)
        {
            trace->pop_front();
            trace->push_back(coords);
        }
        return PART_MOVED;
    }
}

bool Particle::handleCollision(std::vector<Vec3f> collisions, Vec3f attractions)
{
    speed = (speed + attractions).normalize();
    if (!collisions.empty())
    {
        for (auto it = collisions.begin(); it < collisions.end(); ++it)
        {
            Vec3f normal = (*it);
            //material[0][0] = 1.0f;
            //material[0][1] = 0.0f;
            //material[0][2] = 0.0f;
            normal = normal.normalize();
            speed = speed.normalize();
            speed = ((-2.0f) * (normal.dot(speed)) * normal) + speed;
            speed = speed.normalize();
            if (ip_type == IP_COLLISION)
                importantPoint = coords;
        }
        return true;

    }
    return false;
}

const Vec3f Particle::getNext(Vec3f attraction)
{
    return coords + ((speed + attraction).normalize() / speedScaleCoef);
}
#include <QDebug>
void Particle::calcRuntime(std::vector<RuntimeChange>* changes)
{
    for (auto it = changes->begin(); it < changes->end();)
    {
        float cond = 0.0f, sign = 1.0f;
        switch ((*it).rel)
        {
        case PS_POSITIVE: sign = 1.0f; break;
        case PS_NEGATIVE: sign = -1.0f; break;
        default: break;
        }

        if (!(*it).disabled)
        {
            switch ((*it).cond) {
            case PS_DISTANCE:
                cond = (coords - importantPoint).magnitude();
                break;
            case PS_LIFETIME:
                cond = (float)(defLifetime-lifeTime)/ (float)defLifetime;
                break;
            case PS_SPEED_COND:
                cond = (speedScaleCoef - defSpeedScaleCoef)/speedScaleCoef;
                break;
            default:
                break;
            }
        }
        else
            cond = 0.f;
        switch ((*it).param) {
        case PS_SIZE:
            size = defSize + (sign * cond * defSize);
            break;
        case PS_SPEED:
            speedScaleCoef = defSpeedScaleCoef / ( 1.0f + (sign * 1.0f * cond));
            break;
        case PS_BRIGTHNESS:
            material[0][0] = defMaterial[0][0] * ( 1.0f + (sign * cond));
            material[0][1] = defMaterial[0][1] * ( 1.0f + (sign * cond));
            material[0][2] = defMaterial[0][2] * ( 1.0f + (sign * cond));
            break;
        case PS_ALPHA:
            material[0][3] = defMaterial[0][3] * ( 1.0f + (sign * cond));
            break;
        case PS_REDCOLOR:
            material[0][0] = defMaterial[0][0] +( sign * cond);
            break;
        case PS_GREENCOLOR:
            material[0][1] = defMaterial[0][1] + (sign * cond);
            break;
        case PS_BLUECOLOR:
            material[0][2] = defMaterial[0][2] + (sign * cond);
            break;
        default:
            break;
        }
        if ((*it).disabled)
            it = changes->erase(it);
        else
            ++it;
    }

}
